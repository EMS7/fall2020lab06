//1931517
//Estefan Maheux-Saban
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {	
	public void start(Stage stage) {
		Group root = new Group();
		//containers
		HBox hboxChoice = new HBox();
		HBox hboxInfo = new HBox();
		VBox ui = new VBox();
		
		//buttons
		Button btnRock = new Button("Rock");
		Button btnPaper = new Button("Paper");
		Button btnScissor = new Button("Scissors");
		//textfields
		TextField welcome = new TextField("Welcome!");
		TextField win = new TextField("Wins: 0");
		TextField tie = new TextField("Ties: 0");
		TextField loss = new TextField("Losses: 0");
		//action
		RpsGame game = new RpsGame();
		RpsChoice choiceRock = new RpsChoice(welcome,win, loss, tie, "rock", game);
		RpsChoice choicePaper = new RpsChoice(welcome,win, loss, tie, "paper", game);
		RpsChoice choiceScissors = new RpsChoice(welcome,win, loss, tie, "Scissors", game);
		//event
		btnRock.setOnAction(choiceRock);
		btnPaper.setOnAction(choicePaper);
		btnScissor.setOnAction(choiceScissors);
		//add to container
		hboxChoice.getChildren().addAll(btnRock,btnPaper,btnScissor);
		hboxInfo.getChildren().addAll(welcome, win, tie, loss);
		ui.getChildren().addAll(hboxChoice, hboxInfo);
		root.getChildren().add(ui);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    



