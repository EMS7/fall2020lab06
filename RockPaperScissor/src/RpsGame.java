//1931517
//Estefan Maheux-Saban
import java.util.Random;

public class RpsGame {
	private int wins;
	private int losses;
	private int ties;
	private Random rng;
	
	//constructor
	public RpsGame() {
		wins = 0;
		losses = 0;
		ties = 0;
		rng = new Random();
	}
	
	//get methods
	public int getWin() {
		return this.wins;
	}
	
	public int getLosses() {
		return this.losses;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public String playRound(String choice) {
		int cpuChoice = this.rng.nextInt(3);
		switch(cpuChoice) {
		//rock
		case 0:
			if(choice.equals("rock")) {
				this.ties++;
				return "Computer rock plays and the computer ties it!";
			}
			else if(choice.equals("paper")) {
				this.losses++;
				return "Computer rock plays and the computer won! ";
			}
			else {
				this.wins++;
				return "Computer rock plays and the computer lost!";
			}
		//paper
		case 1:
			if(choice.equals("rock")) {
				this.losses++;
				return "Computer paper plays and the computer won!";
			}
			else if(choice.equals("paper")) {
				this.ties++;
				return "Computer paper plays and the computer ties it! ";
			}
			else {
				this.wins++;
				return "Computer paper plays and the computer lost! ";
			}
		//scissors
		case 2:
			if(choice.equals("rock")) {
				this.wins++;
				return "Computer scissors plays and the computer lost!";
			}
			else if(choice.equals("paper")) {
				this.ties++;
				return "Computer plays scissors and the computer ties it!";
			}
			else {
				this.losses++;
				return "Computer scissors plays and the computer won!";
			}
		}
		return "Something is wrong";
	}
}
